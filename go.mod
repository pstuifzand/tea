module code.gitea.io/tea

go 1.12

require (
	code.gitea.io/sdk/gitea v0.12.0
	gitea.com/jolheiser/gitea-vet v0.1.0
	github.com/araddon/dateparse v0.0.0-20200409225146-d820a6159ab1
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/go-git/go-git/v5 v5.0.0
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/olekukonko/tablewriter v0.0.4
	github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966
	github.com/stretchr/testify v1.5.1
	github.com/urfave/cli/v2 v2.2.0
	golang.org/x/crypto v0.0.0-20200429183012-4b2356b1ed79
	golang.org/x/net v0.0.0-20200425230154-ff2c4b7c35a0 // indirect
	golang.org/x/sys v0.0.0-20200430082407-1f5687305801 // indirect
	golang.org/x/tools v0.0.0-20200430040329-4b814e061378 // indirect
	gopkg.in/yaml.v2 v2.2.8
)
